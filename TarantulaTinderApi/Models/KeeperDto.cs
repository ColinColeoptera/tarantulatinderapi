﻿namespace TarantulaTinderApi.Models
{
    public class KeeperDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AboutMe { get; set; }

        public int NumberOfTarantulas
        {
            get { return Tarantulas.Count; }
        }

        public ICollection<TarantulaDto> Tarantulas { get; set; } = new List<TarantulaDto>();
    }
}
