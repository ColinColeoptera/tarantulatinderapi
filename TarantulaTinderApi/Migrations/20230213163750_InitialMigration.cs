﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TarantulaTinderApi.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Keepers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    AboutMe = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Keepers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tarantulas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Genus = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    Species = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    SexualMaturity = table.Column<string>(type: "TEXT", maxLength: 10, nullable: false),
                    KeeperId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tarantulas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tarantulas_Keepers_KeeperId",
                        column: x => x.KeeperId,
                        principalTable: "Keepers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tarantulas_KeeperId",
                table: "Tarantulas",
                column: "KeeperId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tarantulas");

            migrationBuilder.DropTable(
                name: "Keepers");
        }
    }
}
