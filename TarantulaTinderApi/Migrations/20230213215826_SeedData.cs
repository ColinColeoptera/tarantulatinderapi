﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TarantulaTinderApi.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Keepers",
                columns: new[] { "Id", "AboutMe", "Name" },
                values: new object[] { 1, "I'm a tarantula enthusiast and I love to keep them as pets. I have a lot of experience with tarantulas and I'm happy to share my knowledge with you.", "John" });

            migrationBuilder.InsertData(
                table: "Keepers",
                columns: new[] { "Id", "AboutMe", "Name" },
                values: new object[] { 2, "I made this app", "Colin" });

            migrationBuilder.InsertData(
                table: "Keepers",
                columns: new[] { "Id", "AboutMe", "Name" },
                values: new object[] { 3, "I'm a renowned tarantula breeder", "Kelly" });

            migrationBuilder.InsertData(
                table: "Tarantulas",
                columns: new[] { "Id", "Genus", "ImagePath", "KeeperId", "SexualMaturity", "Species" },
                values: new object[] { 1, "Brachypelma", "", 1, "MM", "albopilosum" });

            migrationBuilder.InsertData(
                table: "Tarantulas",
                columns: new[] { "Id", "Genus", "ImagePath", "KeeperId", "SexualMaturity", "Species" },
                values: new object[] { 2, "Brachypelma", "", 2, "MF", "boehmei" });

            migrationBuilder.InsertData(
                table: "Tarantulas",
                columns: new[] { "Id", "Genus", "ImagePath", "KeeperId", "SexualMaturity", "Species" },
                values: new object[] { 3, "Brachypelma", "", 3, "MM", "boehmei" });

            migrationBuilder.InsertData(
                table: "Tarantulas",
                columns: new[] { "Id", "Genus", "ImagePath", "KeeperId", "SexualMaturity", "Species" },
                values: new object[] { 4, "Brachypelma", "", 3, "MF", "albopilosum" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tarantulas",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tarantulas",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tarantulas",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tarantulas",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Keepers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Keepers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Keepers",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
