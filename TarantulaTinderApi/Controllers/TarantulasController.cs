﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using TarantulaTinderApi.Data;
using TarantulaTinderApi.Models;
using TarantulaTinderApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TarantulaTinderApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TarantulasController : ControllerBase
    {
        private readonly ILogger<TarantulasController> _logger;
        private readonly IMailService _mailService;
        private readonly TarantulasDataStore _tarantulasDataStore;
        public TarantulasController(ILogger<TarantulasController> logger, IMailService mailService, TarantulasDataStore tarantulasDataStore)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mailService = mailService ?? throw new ArgumentNullException(nameof(mailService));
            _tarantulasDataStore = tarantulasDataStore ?? throw new ArgumentNullException(nameof(tarantulasDataStore));
        }


        // GET: api/<TarantulaController>
        [HttpGet]
        public ActionResult<IEnumerable<TarantulaDto>> GetAllTarantulas()
        {
            return Ok(_tarantulasDataStore.Tarantulas);
        }

        // GET api/<TarantulaController>/5
        [HttpGet("{id}", Name = "GetTarantulaById")]
        public ActionResult GetTarantulaById(int id)
        {
            try
            {
                if (_tarantulasDataStore.Tarantulas.FirstOrDefault(t => t.Id == id) == null)
                {
                    _logger.LogInformation($"Tarantula with id {id} wasn't found when accessing TarantulasController");
                    return NotFound();
                }

                return Ok(_tarantulasDataStore.Tarantulas.FirstOrDefault(t => t.Id == id));
            }
            catch (Exception e)
            {
                _logger.LogCritical($"Exception while getting tarantula with id {id}.", e);
                throw;
            }
        }

        // POST api/<TarantulaController>
        [HttpPost]
        public ActionResult<TarantulaDto> Post([FromBody] TarantulaCreationDto tarantulaInput)
        {
            try
            {
                var keeper = _tarantulasDataStore.Keepers.FirstOrDefault(o => o.Id == tarantulaInput.KeeperId);

                if (_tarantulasDataStore.Keepers.FirstOrDefault(o => o.Id == tarantulaInput.KeeperId) == null)
                {
                    _logger.LogInformation($"Keeper with id {tarantulaInput.KeeperId} wasn't found when accessing TarantulasController");
                    return Conflict();
                }

                var newTarantulaDto = new TarantulaDto()
                {
                    Id = _tarantulasDataStore.Tarantulas.Max(t => t.Id) + 1,
                    Genus = tarantulaInput.Genus,
                    Species = tarantulaInput.Species,
                    SexualMaturity = tarantulaInput.SexualMaturity,
                    KeeperId = tarantulaInput.KeeperId
                };

                keeper.Tarantulas.Add(newTarantulaDto);


                return CreatedAtRoute("GetTarantulaById", new { id = newTarantulaDto.Id }, newTarantulaDto);

            }
            catch (Exception e)
            {
                _logger.LogCritical($"Exception while creating tarantula.", e);
                throw;
            }
        }

        // PUT api/<TarantulaController>/5
        [HttpPut("{tarantulaId}")]
        public ActionResult<TarantulaDto> Put(int tarantulaId, [FromBody] TarantulaUpdateDto tarantulaInput)
        {
            try
            {
                var tarantula = _tarantulasDataStore.Tarantulas.FirstOrDefault(t => t.Id == tarantulaId);
                KeeperDto keeper = _tarantulasDataStore.Keepers.FirstOrDefault(o => o.Id == tarantulaInput.KeeperId);
                if (tarantula == null)
                {
                    _logger.LogInformation($"Tarantula with id {tarantulaId} wasn't found when accessing TarantulasController");
                    return NotFound();
                }
                var newTarantulaDto = new TarantulaDto()
                {
                    Id = _tarantulasDataStore.Tarantulas.Max(t => t.Id) + 1,
                    Genus = tarantulaInput.Genus,
                    Species = tarantulaInput.Species,
                    SexualMaturity = tarantulaInput.SexualMaturity,
                    KeeperId = tarantulaInput.KeeperId
                };
                tarantula = newTarantulaDto;
                //keeper.Tarantulas.FirstOrDefault(t => t.Id == tarantulaId) = newTarantulaDto;
                //update the tarantula in the Keeper's list
                if (_tarantulasDataStore.Keepers.FirstOrDefault(o => o.Id == tarantulaInput.KeeperId) == null)
                {
                    _logger.LogInformation($"Keeper with id {tarantulaInput.KeeperId} wasn't found when accessing TarantulasController");
                    return Conflict();

                }

                if (keeper.Tarantulas.FirstOrDefault(t => t.Id == tarantulaId) == null)
                {
                    _logger.LogInformation($"Tarantula with id {tarantulaId} wasn't found when accessing TarantulasController");
                    return Conflict();

                }

                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogCritical($"Exception while updating tarantula with id {tarantulaId}.", e);
                throw;
            }
            
        }
        // Patch api/<TarantulaController>/5
        [HttpPatch("{tarantulaId}")]
        public ActionResult<TarantulaDto> Patch(int tarantulaId, [FromBody] JsonPatchDocument<TarantulaUpdateDto> patchDocument)
        {
            var tarantulas = _tarantulasDataStore.Tarantulas;
            if (tarantulas.FirstOrDefault(t => t.Id == tarantulaId) == null)
            {
                _logger.LogInformation($"Tarantula with id {tarantulaId} wasn't found when accessing TarantulasController");
                return NotFound();

            }

            var tarantulaInStore = tarantulas.FirstOrDefault(t => t.Id == tarantulaId);
            var keeper = _tarantulasDataStore.Keepers.FirstOrDefault(o => o.Id == tarantulaInStore.KeeperId);

            if (keeper.Tarantulas.FirstOrDefault(t => t.Id == tarantulaId) == null)
            {
                _logger.LogInformation($"Tarantula with id {tarantulaId} wasn't found when accessing TarantulasController");
                return Conflict();

            }
            
            var newTarantulaUptdate = new TarantulaUpdateDto()
            {
                Genus = tarantulaInStore.Genus,
                Species = tarantulaInStore.Species,
                SexualMaturity = tarantulaInStore.SexualMaturity,
            };

            patchDocument.ApplyTo(newTarantulaUptdate, ModelState);
            if (!ModelState.IsValid)
            {
                _logger.LogInformation($"ModelState invalid for tarantula with ID {tarantulaId} when accessing TarantulasController");
                return BadRequest(ModelState);
            }

            if (!TryValidateModel(newTarantulaUptdate))
            {
                _logger.LogInformation($"Tarantula with id {tarantulaId} wasn't found when accessing TarantulasController");
                return BadRequest(ModelState);
            }

            tarantulaInStore.Genus = newTarantulaUptdate.Genus;
            tarantulaInStore.Species = newTarantulaUptdate.Species;
            tarantulaInStore.SexualMaturity = newTarantulaUptdate.SexualMaturity;
            return NoContent();

        }


        // DELETE api/<TarantulaController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var tarantula = _tarantulasDataStore.Tarantulas.FirstOrDefault(i => i.Id == id);
            if (tarantula == null)
                return NotFound();
            var keeper = _tarantulasDataStore.Keepers.FirstOrDefault(i => i.Tarantulas.Contains(i.Tarantulas.FirstOrDefault(o => o.Id == id)));
            if (keeper == null)
                return NotFound();
            keeper.Tarantulas.Remove(tarantula);
            _tarantulasDataStore.Tarantulas.Remove(tarantula);
            _mailService.Send("Tarantula deleted.", $"Tarantula with id {id} and keeper {keeper.Id} was deleted.");
            return NoContent();
        }
    }
}
