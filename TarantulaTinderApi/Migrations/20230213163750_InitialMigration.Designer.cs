﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using TarantulaTinderApi.Data;

#nullable disable

namespace TarantulaTinderApi.Migrations
{
    [DbContext(typeof(TarantulaTinderContext))]
    [Migration("20230213163750_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "6.0.13");

            modelBuilder.Entity("TarantulaTinderApi.Entities.Keeper", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("AboutMe")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Keepers");
                });

            modelBuilder.Entity("TarantulaTinderApi.Entities.Tarantula", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Genus")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("TEXT");

                    b.Property<int>("KeeperId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("SexualMaturity")
                        .IsRequired()
                        .HasMaxLength(10)
                        .HasColumnType("TEXT");

                    b.Property<string>("Species")
                        .IsRequired()
                        .HasMaxLength(128)
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("KeeperId");

                    b.ToTable("Tarantulas");
                });

            modelBuilder.Entity("TarantulaTinderApi.Entities.Tarantula", b =>
                {
                    b.HasOne("TarantulaTinderApi.Entities.Keeper", "Keeper")
                        .WithMany("Tarantulas")
                        .HasForeignKey("KeeperId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Keeper");
                });

            modelBuilder.Entity("TarantulaTinderApi.Entities.Keeper", b =>
                {
                    b.Navigation("Tarantulas");
                });
#pragma warning restore 612, 618
        }
    }
}
