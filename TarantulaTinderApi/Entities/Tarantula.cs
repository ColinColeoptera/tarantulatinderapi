﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TarantulaTinderApi.Entities
{
    public class Tarantula
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(64)]
        public string Genus { get; set; }
        [Required]
        [MaxLength(128)]
        public string Species { get; set; }
        [MaxLength(10)]
        public string SexualMaturity { get; set; }
        [MaxLength(64)]
        public string ImagePath { get; set; }

        [ForeignKey("KeeperId")]
        public Keeper? Keeper { get; set; }
        public int KeeperId { get; set; }
    }
}
