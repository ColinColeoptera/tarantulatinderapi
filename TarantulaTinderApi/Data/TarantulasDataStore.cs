﻿using TarantulaTinderApi.Models;

namespace TarantulaTinderApi.Data
{
    public class TarantulasDataStore
    {
        //public static TarantulasDataStore Current { get; } = new TarantulasDataStore();
        public List<TarantulaDto> Tarantulas { get; set; }
        public List<KeeperDto> Keepers { get; set; }
        
        public TarantulasDataStore()
        {
            Keepers = new List<KeeperDto>()
            {
                new KeeperDto(){Id = 1, Name = "Colin", AboutMe = "I made this app"},
                new KeeperDto(){Id = 2, Name = "Joe", AboutMe = "I'm Joe Schmoe"},
                new KeeperDto(){Id = 3, Name = "Kelly", AboutMe = "I'm a renowned tarantula breeder"},
            };
            
            Tarantulas = new List<TarantulaDto>()
            {
                new TarantulaDto(){ Id = 1, KeeperId = 1, Genus = "Brachypelma", Species = "albopilosum", SexualMaturity = "MM" },
                new TarantulaDto(){ Id = 2, KeeperId = 2, Genus = "Brachypelma", Species = "boehmei", SexualMaturity = "MM" },
                new TarantulaDto(){ Id = 3, KeeperId = 3, Genus = "Brachypelma", Species = "albopilosum", SexualMaturity = "MF" },
                new TarantulaDto(){ Id = 4, KeeperId = 1, Genus = "Brachypelma", Species = "boehmei", SexualMaturity = "MF" },
            };

            foreach (TarantulaDto tarantula in Tarantulas)
            {
                Keepers.FirstOrDefault(o => o.Id == tarantula.KeeperId).Tarantulas.Add(tarantula);
            }
        }
    }
}
