﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TarantulaTinderApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        private readonly FileExtensionContentTypeProvider _fileExtensionContentTypeProvider;

        public ImagesController(FileExtensionContentTypeProvider fileExtensionContentTypeProvider)
        {
            //_fileExtensionContentTypeProvider = fileExtensionContentTypeProvider;
            _fileExtensionContentTypeProvider = fileExtensionContentTypeProvider ?? throw new ArgumentNullException(nameof(fileExtensionContentTypeProvider));

        }

        //// GET: api/<ImagesController>
        //[HttpGet]
        //public IEnumerable<string> GetAllTarantulaImageUrls()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<ImagesController>/5
        //[HttpGet("{id:int}")]
        //public string GetImageUrl(int id)
        //{
        //    return "value";
        //}

        // GET: api/<ImagesController>
        [HttpGet("tarantula/{id:int}")]
        public ActionResult GetTarantulaImageFile(int id)
        {
            string filePath = $"images/{id}.jpg";
            if (!System.IO.File.Exists(filePath))
                return NotFound();
            //return File(System.IO.File.OpenRead(filePath), "image/jpeg");
            if (!_fileExtensionContentTypeProvider.TryGetContentType(filePath, out var contentType))
                contentType = "application/octet-stream";

            byte[] fileContents = System.IO.File.ReadAllBytes(filePath);
            return File(fileContents, contentType, Path.GetFileName(filePath));
            

        }

        // POST api/<ImagesController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<ImagesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ImagesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

    }
}
