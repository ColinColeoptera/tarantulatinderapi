﻿namespace TarantulaTinderApi.Models
{
    public class TarantulaDto
    {
        public int Id { get; set; }
        public int KeeperId { get; set; }
        public string Genus { get; set; }
        public string Species { get; set; }
        public string SexualMaturity { get; set; }
        //TODO: Add Props for color morph/subspecies, orginal breeder, time since maturity, etc.
        //public int? Legspan { get; set; }
        //Should I Add sexual maturity class that handles sex/isMature or subadult or sling
    }
}
