﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TarantulaTinderApi.Migrations
{
    public partial class TarantulaImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "Tarantulas",
                type: "TEXT",
                maxLength: 64,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "Tarantulas");
        }
    }
}
