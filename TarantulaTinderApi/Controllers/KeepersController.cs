﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using TarantulaTinderApi.Data;
using TarantulaTinderApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TarantulaTinderApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KeepersController : ControllerBase
    {
        private readonly TarantulasDataStore _tarantulasDataStore;
        public KeepersController(TarantulasDataStore tarantulasDataStore)
        {
            _tarantulasDataStore = tarantulasDataStore ?? throw new ArgumentNullException(nameof(tarantulasDataStore));
        }
        // GET: api/<KeepersController>
        [HttpGet]
        public IEnumerable<KeeperDto> GetAllKeeper()
        {
            List<KeeperDto> keepers = _tarantulasDataStore.Keepers;
            for (int i = 0; i < keepers.Count; i++)
                    keepers[i].Tarantulas = _tarantulasDataStore.Tarantulas.Where(t => t.KeeperId == keepers[i].Id).ToList();
            return keepers;
        }

        // GET api/<KeepersController>/5
        [HttpGet("{id}")]
        public KeeperDto Get(int id)
        {
            if (_tarantulasDataStore.Keepers.FirstOrDefault(o => o.Id == id) == null)
            {
                return null;
            }

            KeeperDto keeper = _tarantulasDataStore.Keepers.FirstOrDefault(o => o.Id == id);
            keeper.Tarantulas = _tarantulasDataStore.Tarantulas.Where(t => t.KeeperId == keeper.Id).ToList();
            return keeper;
        }

        // POST api/<KeepersController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<KeepersController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<KeepersController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
