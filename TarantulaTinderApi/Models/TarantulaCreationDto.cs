﻿using System.ComponentModel.DataAnnotations;

namespace TarantulaTinderApi.Models
{
    public class TarantulaCreationDto
    {
        [Required(ErrorMessage = "A valid user is required.")]
        public int KeeperId { get; set; }
        [Required(ErrorMessage = "You should fill out a genus.")]
        [MaxLength(64)]

        public string Genus { get; set; }
        [Required(ErrorMessage = "You should fill out a species.")]

        [MaxLength(128)]

        public string Species { get; set; }
        [Required(ErrorMessage = "You should select a sexual maturity.")]
        [MaxLength(10)]
        public string SexualMaturity { get; set; }
    }
}
