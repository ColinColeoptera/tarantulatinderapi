﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using TarantulaTinderApi.Models;

namespace TarantulaTinderApi.Entities
{
    public class Keeper
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string AboutMe { get; set; }
        public ICollection<Tarantula> Tarantulas { get; set; } = new List<Tarantula>();

        public Keeper(string name)
        {
            Name = name;
        }
    }
}
