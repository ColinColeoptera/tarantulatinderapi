﻿using Microsoft.EntityFrameworkCore;
using TarantulaTinderApi.Entities;

namespace TarantulaTinderApi.Data
{
    public class TarantulaTinderContext : DbContext
    {
        public TarantulaTinderContext(DbContextOptions<TarantulaTinderContext> options) : base(options)
        {
        }

        public DbSet<Keeper> Keepers { get; set; } = null!;
        public DbSet<Tarantula> Tarantulas { get; set; } = null!;
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=../TarantulaTinderApi/Data/TarantulaTinder.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Keeper>()
                .HasData(
                    new Keeper("John")
                    {
                        Id = 1,
                        AboutMe = "I'm a tarantula enthusiast and I love to keep them as pets. I have a lot of experience with tarantulas and I'm happy to share my knowledge with you.",
                    },
                    new Keeper("Colin")
                    {
                        Id = 2,
                        AboutMe = "I made this app",
                    },
                    new Keeper("Kelly")
                    {
                        Id = 3,
                        AboutMe = "I'm a renowned tarantula breeder",
                    }
                );
            modelBuilder.Entity<Tarantula>()
                .HasData(
                    new Tarantula()
                    {
                        Id = 1,
                        KeeperId = 1,
                        Genus = "Brachypelma",
                        Species = "albopilosum",
                        SexualMaturity = "MM",
                        ImagePath = ""
                    },
                    new Tarantula()
                    {
                        Id = 2,
                        KeeperId = 2,
                        Genus = "Brachypelma",
                        Species = "boehmei",
                        SexualMaturity = "MF",
                        ImagePath = ""
                    },
                    new Tarantula()
                    {
                        Id = 3,
                        KeeperId = 3,
                        Genus = "Brachypelma",
                        Species = "boehmei",
                        SexualMaturity = "MM",
                        ImagePath = ""
                    },
                    new Tarantula()
                    {
                        Id = 4,
                        KeeperId = 3,
                        Genus = "Brachypelma",
                        Species = "albopilosum",
                        SexualMaturity = "MF",
                        ImagePath = ""
                    }
                    

                );
            modelBuilder.Entity<Keeper>()
                .HasMany(k => k.Tarantulas)
                .WithOne(t => t.Keeper)
                .HasForeignKey(t => t.KeeperId);

            base.OnModelCreating(modelBuilder);

        }
    }
}
